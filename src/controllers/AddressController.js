import User from "../models/UserModel.js";
import Address from "../models/AddressModel.js";

export default {
  async index(req, res) {
    const { user_id } = req.params;

    const data = await Address.findAll();

    return res.json(data);
  },

  async show(req, res) {
    const { user_id } = req.params;

    const data = await User.findByPk(user_id, {
      include: { association: "addresses" },
    });

    if (!data || data?.addresses?.length === 0) {
      return res.status(400).json({ error: "Address not found" });
    }

    return res.json(data.addresses);
  },

  async store(req, res) {
    const { user_id } = req.params;
    const { zipcode, street, number } = req.body;

    const data = await User.findByPk(user_id);

    if (!data) {
      return res.status(400).json({ error: "User not found" });
    }

    const address = await Address.create({
      zipcode,
      street,
      number,
      user_id,
    });

    return res.json(address);
  },
};
