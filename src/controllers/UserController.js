import User from "../models/UserModel.js";

export default {
  async index(req, res) {
    const data = await User.findAll();

    return res.json(data);
  },

  async show(req, res) {
    const { user_id } = req.params;

    const data = await User.findByPk(user_id);

    if (!data) {
      return res.status(400).json({ error: "User not found" });
    }

    return res.json(data);
  },

  async store(req, res) {
    const { name, email } = req.body;

    const data = await User.create({ name, email });

    return res.json(data);
  },
};
