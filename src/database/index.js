import { Sequelize } from "sequelize";

import dbConfig from "./config/indexES6.js";

import User from "../models/UserModel.js";
import Address from "../models/AddressModel.js";
import Tech from "../models/TechModel.js";

const connection = new Sequelize(dbConfig);

User.init(connection);
Address.init(connection);
Tech.init(connection);

User.associate(connection.models);
Address.associate(connection.models);
Tech.associate(connection.models);

export default connection;
