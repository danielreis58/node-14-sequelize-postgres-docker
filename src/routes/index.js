import express from "express";

import addressRoute from "./AddressRoute.js";
import userRoute from "./UserRoute.js";
import techRoute from "./TechRoute.js";

const routes = express.Router();

routes.use(addressRoute);
routes.use(userRoute);
routes.use(techRoute);

export default routes;
