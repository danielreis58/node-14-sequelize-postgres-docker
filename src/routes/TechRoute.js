import express from "express";
import TechController from "../controllers/TechController.js";

const routes = express.Router();

routes.get("/users/:user_id/techs", TechController.index);
routes.post("/users/:user_id/techs", TechController.store);
routes.delete("/users/:user_id/techs", TechController.delete);

export default routes;
