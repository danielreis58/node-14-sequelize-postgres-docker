import express from "express";
import AddressController from "../controllers/AddressController.js";

const routes = express.Router();

routes.get("/addresses", AddressController.index);
routes.get("/users/:user_id/addresses", AddressController.show);
routes.post("/users/:user_id/addresses", AddressController.store);

export default routes;
