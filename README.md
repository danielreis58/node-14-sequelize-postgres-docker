# Description
This is a project based on:

[SQL no Node.js com Sequelize | Masterclass #01](https://www.youtube.com/watch?v=Fbu7z5dXcRs&list=WL&index=3&t=274s&ab_channel=Rocketseat),

[Criando um ambiente de API com NodeJs + MongoDB utilizando Docker e Docker compose](https://www.youtube.com/watch?v=z4Vmpc1BOx0&list=WL&index=3&t=1188s&ab_channel=CodarMe)

To run this application locally follow the steps below

### 1. Install from dependencies

Inside the project folder, run the command

```shell
npm install
```

### 2. Upload a docker instance

Inside the project folder, run the command

```shell
make up
```

To see docker logs, run the command (optional):

```shell
make logs
```

To kill docker instance, run the command (optional):

```shell
make down
```

### 3. Run API

Inside the project folder, run the command

```shell
npm run dev
```

### 4. Run database migrations

Inside the project folder, run the command

```shell
npm run migrate
```

There`s a Postman collection file (postmanCollection.json) to facilitate testing
